import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthorizationService } from './../../services/authorization.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';


let focussed = false;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  demoPasswords: string[] = ['super', 'agency', 'hr'];
  hide = true;
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private authService: AuthorizationService, private router: Router,
              public dialog: MatDialog) {

    this.loginForm = formBuilder.group({
      ID: new FormControl('', [Validators.required, Validators.max(999999)]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
  }
  getIDError(): string {
    if (this.loginForm.controls.ID.hasError('required')) {
      return 'You must enter a value';
    }
    if (this.loginForm.controls.ID.hasError('max')) {
      return 'You must enter a  6 digit value';
    }

    return '';
  }
  getPasswordError(): string {
    if (this.loginForm.controls.password.hasError('required')) {
      return 'You must enter a value';
    }
    return '';
  }
  login(): void {
    this.loginForm.markAllAsTouched();
    const { ID, password } = this.loginForm.value;
    if (this.loginForm.status !== 'INVALID') {
      if (this.demoPasswords.includes(password)) {
        if (ID.toString().length > 2 && password === 'agency') {
          focussed = false;
          this.onFocus();
        } else if (ID.toString().length !== 6 && (password === 'hr' || password === 'super')) {
          focussed = false;
          this.onFocus();
        } else {
          this.authService.login(ID, password);
          this.router.navigate(['home']);
        }
      } else {
        focussed = false;
        this.onFocus();
      }
    } else {
      focussed = false;
      this.onFocus();
    }
  }
  onFocus(): void {
    if (!focussed) {
      const dialogRef = this.dialog.open(DemoModalComponent, {
        width: '500px',
        disableClose: true
      });
    }
  }
}


@Component({
  selector: 'app-demo-modal',
  template: `<h1 mat-dialog-title class="text-center">Please follow Demo Instructions</h1>
<mat-dialog-content>
  <h4>i. For Supervisor/Manager access:</h4>
  <p><b>ID: </b> Enter 6 digit BadgeID & <b>Password:</b> super</p>
  <h4>ii. For HR access:</h4>
  <p><b>ID: </b> Enter 6 digit BadgeID & <b>Password:</b> hr</p>
  <h4>iii. For Agency access:</h4>
  <p><b>ID:</b>  Enter Agency ID between 1-26 & <b>Password:</b> agency</p>
</mat-dialog-content>
<mat-dialog-actions>
  <button mat-button (click)="onNoClick()">Okay</button>
</mat-dialog-actions>`,

})
export class DemoModalComponent {
  constructor(
    public dialogRef: MatDialogRef<DemoModalComponent>) { }
  onNoClick(): void {
    focussed = true;
    this.dialogRef.close();
 }

}
