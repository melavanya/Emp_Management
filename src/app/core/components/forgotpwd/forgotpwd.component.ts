import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';



@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.scss']
})
export class ForgotpwdComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  getErrorMessage(): string {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
  retrieve(): void {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '300px',
      disableClose: true
    });
  }
 }


@Component({
  selector: 'app-confirmation-modal',
  template: `<h1 mat-dialog-title>Email Sent</h1>
<mat-dialog-content>
  <p>You will receive a response with password reset instructions.</p>
</mat-dialog-content>
<mat-dialog-actions>
  <button mat-button [mat-dialog-close]="true">Okay</button>
</mat-dialog-actions>`,
styleUrls: ['./forgotpwd.component.scss']

})
export class ConfirmationModalComponent {
  constructor(
    public dialogRef: MatDialogRef<ConfirmationModalComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
