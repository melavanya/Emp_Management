import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../shared/material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { ChartistModule } from 'ng-chartist';
import { StickerComponent } from './dashboard-components/sticker/sticker.component';
import { MycandidatesComponent } from './dashboard-components/mycandidates/mycandidates.component';
import { StatusComponent } from './dashboard-components/status/status.component';
import { AgenciesGraphComponent } from './dashboard-components/agencies-graph/agencies-graph.component';
import { CandidatesGraphComponent } from './dashboard-components/candidates-graph/candidates-graph.component';
import { DepartmentGraphComponent } from './dashboard-components/department-graph/department-graph.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [DashboardComponent, StickerComponent, MycandidatesComponent, StatusComponent, AgenciesGraphComponent, CandidatesGraphComponent, DepartmentGraphComponent]
})
export class DashboardModule {}
