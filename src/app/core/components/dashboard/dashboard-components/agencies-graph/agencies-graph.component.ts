
import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist';

declare var require: any;
const data = require('./data.json');

export interface Chart {
	type: ChartType;
	data: Chartist.IChartistData;
	options?: any;
	responsiveOptions?: any;
	events?: ChartEvent;
}


@Component({
	selector: 'app-agencies-graph',
	templateUrl: './agencies-graph.component.html',
	styleUrls: ['./agencies-graph.component.scss']
})
export class AgenciesGraphComponent implements OnInit {
	barChart1: Chart = {
		type: 'Bar',
		data: data['Bar'],
		options:
		{
			seriesBarDistance: 10,
			onlyInteger: true,
			high: 20,
			reverseData: true,
			horizontalBars: true,
			axisY: {
				showGrid: false,
				offset: 70
			},
			axisX: {
				showGrid: false,
				onlyInteger: true,
				divisor: 5
			},
			height: 360
		},

		responsiveOptions: [
			[
				'screen and (min-width: 640px)',
				{
					axisX: {
						labelInterpolationFnc: function (value: number, index: number): string {
							return index % 1 === 0 ? `${value}` : '';
						}
					}
				}
			]
		]
	};


	constructor() { }

	ngOnInit(): void {
	}

}
