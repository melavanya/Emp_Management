
import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist';

declare var require: any;
const data = require('./data.json');

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}


@Component({
  selector: 'app-department-graph',
  templateUrl: './department-graph.component.html',
  styleUrls: ['./department-graph.component.scss']
})
export class DepartmentGraphComponent implements OnInit {

  barChart1: Chart = {
    type: 'Bar',
    data: data['Bar'],
    options: {
      seriesBarDistance: 15,
      high: 15,
      axisX: {
        offset: 20,
        showGrid: false

      },
      axisY: {
        offset: 40,
        onlyInteger: true,
        divisor: 5,
        showGrid: false
      },
      height: 360
    },

    responsiveOptions: [
      [
        'screen and (min-width: 640px)',
        {
          axisX: {
            labelInterpolationFnc: function (value: number, index: number): string {
              return index % 1 === 0 ? `${value}` : '';
            }
          }
        }
      ]
    ]
  };


  constructor() { }

  ngOnInit(): void {
  }

}
