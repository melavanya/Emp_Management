import { Component, OnInit } from '@angular/core';
import {Candidate,candidates} from '../mycandidates/candidates-data';

@Component({
  selector: 'app-mycandidates',
  templateUrl: './mycandidates.component.html',
  styleUrls: ['./mycandidates.component.scss']
})
export class MycandidatesComponent implements OnInit {

  candidatesData:Candidate[];
  constructor() {
    this.candidatesData = candidates;
   }

  ngOnInit(): void {
  }

}
