export interface Candidate {
    image: string;
    class: string;
    name: string;
    email: string;
}

export const candidates: Candidate[] = [
    {
        image: 'assets/images/users/profile.png',
        class: 'online',
        name: 'FName LName',
        email: 'test@test.com'
    },
    {
        image: 'assets/images/users/profile.png',
        class: 'busy',
        name: 'FName LName',
        email: 'test@test.com'
    },
    {
        image: 'assets/images/users/profile.png',
        class: 'offline',
        name: 'FName LName',
        email: 'test@test.com'
    },
    {
        image: 'assets/images/users/profile.png',
        class: 'online',
        name: 'FName LName',
        email: 'test@test.com'
    },
    {
        image: 'assets/images/users/profile.png',
        class: 'busy',
        name: 'FName LName',
        email: 'test@test.com'
    },
]