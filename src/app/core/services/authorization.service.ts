import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  badgeID: any;
  role: any = 'super';
  constructor(private http: HttpClient) { }

  isAuthorized(allowedRoles: string[]): boolean {
    // check if the list of allowed roles is empty, if empty, authorize the user to access the page
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }
  // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
    return allowedRoles.includes(this.role);
  }
  getBadgeID = () => this.badgeID;
  // login call
  login = (ID: any, password: any) =>  {
    this.badgeID = ID;
    this.role = password;
  }
}
