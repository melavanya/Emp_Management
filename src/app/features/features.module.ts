
import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../shared/material-module';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FeatureRoutes } from './features.routing';

import { RequisitionsComponent } from './requisitions/requisitions.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { AgenciesComponent } from './agencies/agencies.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { CreatePositionComponent } from './create-position/create-position.component';
import { ChangePositionComponent } from './change-position/change-position.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FeatureRoutes),
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule
  ],
  providers: [],
  entryComponents: [],
  declarations: [
    AssignmentsComponent,
    CandidatesComponent,
    RequisitionsComponent,
    AgenciesComponent,
    OnboardingComponent,
    CreatePositionComponent,
    ChangePositionComponent
  ]
})
export class FeaturesComponentsModule {}
