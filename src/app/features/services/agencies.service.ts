import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AgenciesService {
  baseURL = environment.baseURL;

  constructor(private http: HttpClient) { }

  getCandidates = (agencyID: any) => this.http.get(this.baseURL + 'candidate/agency/' + agencyID);

  createCandidate = (agencyID: any, details: any) => this.http.post(this.baseURL + 'candidate/agency/' + agencyID, details);

  editCandidate = (agencyID: any, details: any) => this.http.put(this.baseURL + 'candidate/agency/' + agencyID, details);

  getRequisitions = (agencyID: any) => this.http.get(this.baseURL + 'agency-request/agency/' + agencyID);

  getReqCandidates = (agencyID: any, reqID: any) => this.http.get(this.baseURL + 'candidate/agency/' + agencyID + '/' + reqID);

  addReqCandidate = (agencyID: any, reqID: any, candidateID: any) => this.http.post(this.baseURL + 'request-candidate/agency/' +
    agencyID + '/' + candidateID + '/' + reqID, {})

  removeReqCandidate = (agencyID: any, reqID: any, candidateID: any) => this.http.delete(this.baseURL + 'request-candidate/agency/' +
  agencyID + '/' + candidateID + '/' + reqID )
}
