import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HrService {
  baseURL = environment.baseURL;

  constructor(private http: HttpClient) { }

  getAgencies = () => this.http.get(this.baseURL + 'agency-list/admin');

  getRequisitions = () => this.http.get(this.baseURL + 'staff-request/admin');

  editAgencyInfo = (agencyID: any, details: any) => this.http.put(this.baseURL + 'agency-list.admin/' + agencyID, details);

  approveRequisition = (badgeID: any, reqID: any) => this.http.put(this.baseURL + 'valid-request/admin/' +
    badgeID + '/' + reqID, {})

  getRequisitionAgencyInfo = (reqID: any) => this.http.get(this.baseURL + 'agency-request/admin/' + reqID);

  addAgency = (badgeID: any, agencyId: any, reqID: any) => this.http.post(this.baseURL + 'agency/admin/' +
    badgeID + '/' + agencyId + '/' + reqID, {})

  removeAgency = (badgeID: any, agencyId: any, reqID: any) => this.http.delete(this.baseURL + 'agency/admin/' +
    badgeID + '/' + agencyId + '/' + reqID, {})

  editRequisition = (badgeID: any, reqID: any) => this.http.put(this.baseURL + 'request/admin/' + badgeID, +'/' + reqID);

  deleteRequisition = (badgeID: any, reqID: any) => this.http.delete(this.baseURL +
      'request/admin' + badgeID, +'/' + reqID)

}
