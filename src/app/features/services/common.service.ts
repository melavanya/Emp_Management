import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { RequiredValidator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  baseURL = environment.baseURL;
  constructor(private http: HttpClient) { }

  getJobs = () => this.http.get(this.baseURL + 'jobs');

  getReasons = () => this.http.get(this.baseURL + 'reasons');

  editRequisition = (reqID: any, details: any) => this.http.put(this.baseURL + 'staff-request/request-manager/' + reqID, details);

  createRequisition = (badgeID: any, details: any) => this.http.post(this.baseURL + 'staff-request/request-manager/' +
    badgeID, details)

}
