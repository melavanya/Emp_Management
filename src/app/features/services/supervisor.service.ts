import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SupervisorService {
  baseURL = environment.baseURL;
  constructor(private http: HttpClient) { }

  getRequisitions = (badgeID: any) => this.http.get(this.baseURL + 'staff-request/request-manager/' + badgeID);

  getPendingApprovals = (badgeID: any) => this.http.get(this.baseURL + 'staff-request/approve-manager/' + badgeID);

  approveRequisition = (badgeID: any, reqID: any) => this.http.put(this.baseURL + 'valid-request/request-manager/' +
    badgeID + '/' + reqID, {})

}
