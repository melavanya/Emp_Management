import { Renderer2, Inject, Component, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {
  constructor(
    private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: any
  ) {
  }
  ngOnInit() {
    const s = this.renderer2.createElement('script');
    s.type = 'text/javascript';
    s.src = 'https://assets.calendly.com/assets/external/widget.js';
    s.text = ``;
    this.renderer2.appendChild(this._document.body, s);
  }
  
  startCalendly() {
    const iframe: HTMLIFrameElement | null = document.querySelector('.calendly-inline-widget iframe');
    if (iframe) {
      iframe.src = iframe.src;
    }
  }
}