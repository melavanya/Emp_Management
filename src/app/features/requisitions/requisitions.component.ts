
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';


const NAMES = [
  {
    name: 'test',
    email: 'test@test.com',
  },
  {
    name: 'test',
    email: 'test@test.com'
  },
  {
    name: 'test',
    email: 'test@test.com'
  },
  {
    name: 'test',
    email: 'test@test.com'
  }
];

@Component({
  selector: 'app-requisitions',
  templateUrl: './requisitions.component.html',
  styleUrls: ['./requisitions.component.scss']
})
export class RequisitionsComponent implements AfterViewInit {

  displayedColumns: string[] = ['name', 'email', 'actions'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor() {
    this.dataSource = new MatTableDataSource(NAMES);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
