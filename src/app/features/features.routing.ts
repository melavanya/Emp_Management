
import { Routes } from '@angular/router';
import { RequisitionsComponent } from './requisitions/requisitions.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { AgenciesComponent } from './agencies/agencies.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { ChangePositionComponent } from './change-position/change-position.component';
import { CreatePositionComponent } from './create-position/create-position.component';



export const FeatureRoutes: Routes = [
  {
    path: 'agencies',
    component: AgenciesComponent,
    // data: {
    //   allowedRoles: ['hr']
    // }
  },
  {
    path: 'candidates',
    component: CandidatesComponent,
    // data: {
    //   allowedRoles: ['agency']
    // }
  },
  {
    path: 'requisitions',
    component: RequisitionsComponent,
    // data: {
    //   allowedRoles: ['super','hr']
    // }
  },
  {
    path: 'assignments',
    component:AssignmentsComponent,
    // data: {
    //   allowedRoles: ['agency']
    // }
  },
  {
    path: 'schedule',
    component: OnboardingComponent,
  },
  {
    path: 'create',
    component: CreatePositionComponent,
  },
  {
    path: 'change',
    component:ChangePositionComponent,
  }
  ];
