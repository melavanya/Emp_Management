import { Routes } from '@angular/router';

import { LoginComponent } from './core/components/login/login.component';
import { FullComponent } from './core/components/layouts/full/full.component';
import { ForgotpwdComponent } from './core/components/forgotpwd/forgotpwd.component';
import { AuthorizationGuard } from './core/guards/authorization.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'forgot',
    component:ForgotpwdComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: FullComponent,
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./features/features.module').then(m => m.FeaturesComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./core/components/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
    ]
  }];
