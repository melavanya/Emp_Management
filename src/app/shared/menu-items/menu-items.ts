import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  group: string[];
  icon: string;
}

const MENUITEMS = [
  { state: 'dashboard', name: 'Dashboard', icon: 'av_timer', group: ['hr', 'super', 'agency'] },
  { state: 'requisitions', name: 'Requisitions', icon: 'group_add', group: ['hr', 'super'] },
  { state: 'candidates', name: 'Candidates', icon: 'manage_accounts', group: ['agency'] },
  { state: 'agencies', name: 'Agencies', icon: 'business', group: ['hr'] },
  { state: 'assignments', name: 'Assignments', icon: 'assignment', group: ['agency']},
  { state: 'schedule', name: 'Scheduler', icon: 'edit_calendar', group: ['hr', 'agency'] },
  { state: 'create', name: 'Create Position', icon: 'work', group: ['super', 'hr'] },
  { state: 'change', name: 'Change Status', icon: 'verified_user', group: ['hr'] },
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
